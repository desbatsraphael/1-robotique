#!/usr/bin/python3

import time
import numpy
import pypot.dynamixel
import math

dxl_io = pypot.dynamixel.DxlIO('/dev/ttyACM1', baudrate=1000000)

# Length of the different parts of the leg
L1 = 53                                                    
L2 = 66                                                   
L3 = 130  

# Theta Coordinates
theta1 = float(input("theta1 = "))
theta2 = float(input("theta2 = "))
theta3 = float(input("theta3 = "))

a=math.cos(theta1)*L1
b=math.sin(theta1)*L1

# Change degrees in radians
theta1 = math.radians(theta1)
theta2 = math.radians(theta2)
theta3 = math.radians(theta3)
thetaa = math.radians(a)
thetab = (math.radians(90) + thetaa - (math.radians(b)))

thetac = theta2 - thetaa
thetad = -(theta3 - thetab)

# Coordinates computing x, y and z
x = math.cos(theta1)*(L1+L2*math.cos(thetac)+L3*math.cos(thetac + thetad))
y = math.sin(theta1)*(L1+L2*math.cos(thetac)+L3*math.cos(thetac + thetad))  
z = -(L2*math.sin(thetac)+L3*math.sin(thetac+thetad))

# Display x, y, z
print("x = ", x)
print("y = ", y)
print("z = ", z)

dxl_io.set_goal_position({11:theta1, 12:theta2, 13:theta3})
