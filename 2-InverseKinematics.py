#!/usr/bin/python3

import math

def legDK(P3x, P3y, P3z, L1, L2, L3):

	d13 = math.sqrt(P3x**2 + P3y**2) - L1
	d = math.sqrt(d13**2 + P3z**2)
	a = math.atan(P3z / d13)
	b = math.acos((L2**2 + d**2 - L3**2) / (2 * L2 * d))
	c = math.acos((L3**2 + L2**2 - d**2) / (2 * L3 * L2))
	Theta1 = math.atan(P3y / P3x)
	Theta2 = a + b
	Theta3 = math.pi - c

	return Theta1, Theta2, Theta3

def main():
	
	Theta1, Theta2, Theta3 = legDK(118.79, 0, -115.14, 51, 63.7, 93)
	print("Theta1 = "+str(Theta1))
	print("Theta2 = "+str(Theta2))
	print("Theta3 = "+str(Theta3))

main()