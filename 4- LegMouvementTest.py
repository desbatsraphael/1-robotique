import time
import numpy
import pypot.dynamixel
import math

dxl_io = pypot.dynamixel.DxlIO('/dev/ttyACM0', baudrate=1000000)

while True:
    t = time.time()
    a = 20*math.sin(2*math.pi*0.5*t)
    b = 20*math.sin(2*math.pi*0.25*t)
    c = 20*math.sin(2*math.pi*1*t)
    dxl_io.set_goal_position({11:a, 12:b, 13:c})

