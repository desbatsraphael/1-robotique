#!/usr/bin/python3

import time
import numpy
import pypot.dynamixel
import math

dxl_io = pypot.dynamixel.DxlIO('/dev/ttyACM1', baudrate=1000000)

# Length of the different parts of the leg
L1 = 53                                                    
L2 = 66                                                   
L3 = 130  

# Positions Coordinates / float type
print("Enter values :")
P3x = float(input("P3x = "))                                
P3y = float(input("P3y = "))
P3z = float(input("P3z = "))

# Equation for find coordinates
Lproj = math.sqrt(P3x**2 + P3y**2)
d13 = math.sqrt(P3x**2 + P3y**2) - L1
d = math.sqrt(P3z**2 + d13**2)
alpha = math.degrees(math.atan((P3z)/(d13)))
beta = math.degrees(math.acos((L2**2 + d**2 - L3**2)/(2*L2*d)))
theta4 = (math.acos((L2**2+L3**2-d**2)/(2*L2*L3)))*180/math.pi

# Alpha, Beta and Theta angle
a = math.atan(P3z / d13)                                                    
b = math.acos((L2**2 + d**2 - L3**2) / (2 * L2 * d))                                                       
theta = math.acos((L3**2 + L2**2 - d**2) / (2 * L3 * L2))

# Coordinates computing theta1, theta2 and theta3
theta1 = math.degrees(math.acos((P3x)/(Lproj)))
theta2 = -(alpha + beta - a)
theta3 = -(180 - theta4 - theta)

dxl_io.set_goal_position({11:theta1, 12:theta2, 13:theta3})
