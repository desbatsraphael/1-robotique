#!/usr/bin/python3

import math

def legDK(theta1, theta2, theta3, L1, L2, L3):

	X1=math.cos(theta1)*L1
	Y1=math.sin(theta1)*L1
	Z1=0

	X2=(L1+L2*math.cos(theta2))*math.cos(theta1)
	Y2=(L1+L2*math.cos(theta2))*math.sin(theta1)
	Z2=(L2*math.sin(theta2))+Z1

	X3=(L1+L2*math.cos(theta2))+L3*math.cos(theta4)*math.cos(theta1)
	Y3=(L1+L2*math.cos(theta2))+L3*math.cos(theta4)*math.sin(theta1)
	Z3=(L3*math.sin(theta4))+Z2

	return X1,Y1,Z1,X2,Y2,Z2,X3,Y3,Z3


def main():

	X1,Y1,Z1,X2,Y2,Z2 = legDK(2,4,0,51,63,93)
	print("X1="+str(X1))
	print("Y1="+str(Y1))
	print("Z1="+str(Z1))
	print("X2="+str(X2))
	print("Y2="+str(Y2))
	print("Z2="+str(Z2))
	print("X3="+str(X3))
	print("Y3="+str(Y3))
	print("Z3="+str(Z3))

main()